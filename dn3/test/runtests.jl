using Test, Domaca03

@testset "Butcher" begin
    Euler = Butcher(Vector{Float64}(), [1.0])
    B4    = Butcher([0.5, 0, 0.5, 0, 0, 1], [1/6, 1/3, 1/3, 1/6])
    Heun  = Butcher(
        [1.0],
        [0.5, 0.5],
        [1.0, 0.0]
    )

    @test length(Euler.c) == 0
    @test length(B4.c) == 3
    @test length(Heun.c) == 1
    
    @test B4.c[1] ≈ 0.5
    @test B4.c[2] ≈ 0.5
    @test B4.c[3] ≈ 1.0

    @test Heun.c[1] ≈ 1.0
end

@testset "Runge-Kutta" begin
    Euler = Butcher(Vector{Float64}(), [1.0])
    B4    = Butcher([0.5, 0, 0.5, 0, 0, 1], [1/6, 1/3, 1/3, 1/6])
    Heun  = Butcher(
        [1.0],
        [0.5, 0.5],
        [1.0, 0.0]
    )

    F(t, (x,dx)) = [dx, 3]
    _, EX  = runge_kutta(Euler, F, (0.0, [0.0, 0.0]), (0, 1), 1000)
    _, B4X = runge_kutta(B4,    F, (0.0, [0.0, 0.0]), (0, 1), 100)
    _, _, HX  = runge_kutta(Heun,  F, (0.0, [0.0, 0.0]), 1, 1e-4, 0.95) 

    @test abs(EX[end,1] - B4X[end,1]) < 1e-2
    @test EX[end,2] ≈ B4X[end,2]
    @test abs(EX[end,1] - HX[end,1]) < 1e-2
end