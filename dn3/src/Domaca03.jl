module Domaca03

export Butcher, runge_kutta

# -------------------- [ Butcher ] ---------------------- #

struct Butcher{T}
    a :: Vector{T}
    b :: Vector{T}
    b_ :: Vector{T}
    e :: Vector{T}
    c :: Vector{T}
end

function Butcher(a :: Vector{T}, b :: Vector{T}, b_ :: Vector{T}) :: Butcher{T} where {T}
    s = length(b)
    c = zeros(s-1)
    i = 1
    for j = 1 : s-1
        c[j] = sum(a[i:i+j-1])
        i += j
    end
    return Butcher(copy(a), copy(b), copy(b_), b .- b_, c)
end


function Butcher(a :: Vector{T}, b :: Vector{T}) :: Butcher{T} where {T}
    return Butcher(a, b, zero(b))
end

# -------------------- [ Runge Kutta ] ------------------ #

function runge_kutta(B, f, (x0, y0), (a, b), n)
    h = (b - a) / n
    s = length(B.b)
    
    K = zeros((s,   length(y0)))
    X = zeros((n+1, length(x0)))
    Y = zeros((n+1, length(y0)))

    X[1,:] .= x0
    Y[1,:] .= y0
    for p = 1 : n
        xp  = @view X[p,:]
        yp  = @view Y[p,:]

        K[1,:] = h .* f(xp, yp)
        Yp1    = yp + B.b[1] .* @view K[1,:]

        j = 1
        for i = 2 : s

            x      = xp .+ B.c[i-1] * h
            y      = yp .+ sum((@view B.a[j : j + (i-2)]) .* (@view K[1:i-1,:]), dims=1)
            K[i,:] = h * f(x,y)

            Yp1 = Yp1 .+ B.b[i] * @view K[i,:]
            j += i-1
        end

        Y[p+1,:] = Yp1
        X[p+1,:] = xp .+ h
    end

    return (X,Y)
end


function norm(x)
    return sqrt(mapreduce(abs2, +, x))
end

function to_matrix(x)
    return mapreduce(permutedims, vcat, x)
end

function runge_kutta(B, f, (x0, y0), b, eps, sigma)
    h = (b - x0) * sqrt(eps)
    s = length(B.b)
    
    K = zeros((s,length(y0)))
    X = [x0]
    Y = [y0]
    H = [h]

    xn = x0
    yn = y0
    while xn < b

        K[1,:] .= h .* f(xn, yn)
        j = 1
        for i = 2 : s
            x      = xn .+ B.c[i-1] * h
            y      = yn .+ sum((@view B.a[j : j + (i-2)]) .* (@view K[1:i-1]), dims=1)
            K[i,:] .= h .* f(x,y)
            j += i-1
        end

        e = norm(sum(B.e .* K, dims=1))
        if h < (b-x0)*eps || e < eps * h
            yn = yn .+ @view sum(B.b .* K, dims=1)[:]
            push!(Y, yn)
            xn = xn .+ h
            push!(X, xn)
            if e * 10 < eps * h
                h = h * sigma * (eps * h / e)^(1/5)
            end
            if xn + h > b
                h = b-xn
            end
        else
            h = h/2
        end
        push!(H, h)
    end

    return (H,X,to_matrix(Y))
end

# ------------------------------------------------------- #

end # module Domaca03