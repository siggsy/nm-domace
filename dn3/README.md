# Domača naloga 3: Obhod Lune

<ins>Avtor:</ins> Žiga Leskovec

<ins>Datum:</ins> 18. 6. 2024

## Uvod

V domači nalogi je bilo potrebno rečunati sistem diferencialnih enačb za pot sonde na tiru iz Zemljine orbite z vrnitvijo brez potiska, ki obkorži Luno in se vrne nazaj v Zemljino orbito.

Prvi del poročila ponovi definicijo problema in pretvori sistem diferencialnih enačb 2. reda v sistem diferencialnih enačb 1. reda.

V drugem delu se osredotočimo na računanje sistema diferencialnih enačb.
V njem na grobo opišemo implementacijo Butcherjevih tabel in kako z njimi implementiramo Runge-Kutta metode poljubnega reda.

Na koncu je tudi narisana pot sonde ki obkroži Luno in se vrne nazaj na Zemljino orbito.

## Projekt

- [docs](./docs): generiran pdf poročila z uporabo `jupyter notebook`
- [scripts](./scripts): testne skripte in poročilo napisano v `jupyter notebook`
- [src](./src): izvorna koda. V [Domaca03.jl](./src/Domaca03.jl) se nahaja izvorna koda domače naloge
- [test](./test): testi za domačo nalogo. Poženejo se tako kot pri vajah (`test Domaca03` v pkg)

IJulia kernel rabi bit nastavljen da uporabi lokalno okolje, saj samo tako lahko uporabimo `using Domaca03`.
V dokumentu [porocilo.ipynb](./scripts/porocilo.ipynb) se nahaja demo implementacije domače naloge.
