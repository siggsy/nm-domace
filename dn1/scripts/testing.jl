using Domaca01
import LinearAlgebra: lu, NoPivot, I

A = [  4  1 -2  2
     ;  1  2  0  1
     ; -2  0  3 -2
     ;  2  1 -2 -1
     ]

T, Q = tridiag(A)
T * Matrix{Float64}(I,4,4)

isapprox(T, Q' * A * Q)
