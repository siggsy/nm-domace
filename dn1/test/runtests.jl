using Test, Domaca01

@testset "Tridiagonalna matrika" begin

    @testset "getindex" begin
        n = 3
        T = Tridiagonalna([1; 2; 3], [3; 4])
        M = [[1; 3; 0] [3; 2; 4] [0; 4; 3]]

        @test T ≈ M
    end


    @testset "setindex!" begin
        T = Tridiagonalna([1; 2; 3], [3; 4])
        T[1,1] = 2
        T[1,2] = 6
        T[2,1] = 6
        @test T[1,1] == 2
        @test T[1,2] == 6
        @test T[2,1] == 6

        @test_throws BoundsError T[3,1] = -1
    end


    @testset "first/last tindex" begin
        T = Tridiagonalna([1; 2; 3], [3; 4])
        @test firstindex(T) == 1
        @test lastindex(T) == 9
    end


    @testset "multiplication" begin
        T = Tridiagonalna([1; 2; 3], [3; 4])
        M = [[1; 3; 0] [3; 2; 4] [0; 4; 3]]
        x = [1,2,3]

        @test M * M == T * M
        @test M * x == T * x
    end

    @testset "Symmetric Hessenberg" begin
        A = [  4  1 -2  2
            ;  1  2  0  1
            ; -2  0  3 -2
            ;  2  1 -2 -1
            ]

        T, Q = tridiag(A)
        _A = Q * (T * Q')
        @test A ≈ _A
    end

end



@testset "Inverzna potenčna metoda" begin

    @testset "LU razcep" begin
        T = Tridiagonalna{Float64}([1; 2; 3], [4; 5])
        L, U = lu(T)

        @test L ≈ [ 1      0  0
                  ; 4      1  0
                  ; 0  -5/14  1
                  ]
        @test U ≈ [ 1    4      0
                  ; 0  -14      5
                  ; 0    0  67/14
                  ]
    end

    @testset "Operator \\" begin
        T    = Tridiagonalna{Float64}([1; 2; 3], [4; 5])
        L, U = lu(T)
        b    = [1.0; 2.0; 3.0]

        x = L\b
        y = U\b

        @test L*x ≈ b
        @test U*y ≈ b
    end

    @testset "Inverzna potenčna iteracija" begin
        A = [  4  1 -2  2
            ;  1  2  0  1
            ; -2  0  3 -2
            ;  2  1 -2 -1
            ]

        λ1, v1 = inv_lastni(A, -2)
        λ2, v2 = inv_lastni(A,  1)
        λ3, v3 = inv_lastni(A,  2)
        λ4, v4 = inv_lastni(A,  7)

        @test λ1 ≈ -2.19751697 && A*v1 ≈ λ1*v1
        @test λ2 ≈  1.08436446 && A*v2 ≈ λ2*v2
        @test λ3 ≈  2.26853140 && A*v3 ≈ λ3*v3
        @test λ4 ≈  6.84462110 && A*v4 ≈ λ4*v4
    end

end
