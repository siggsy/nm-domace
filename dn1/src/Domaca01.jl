module Domaca01

export Tridiagonalna, tridiag, inv_lastni, lu, to_matrix
import Base: getindex, setindex!, firstindex, lastindex, *, \, size, isapprox, Matrix
import LinearAlgebra: I, normalize, norm


struct Tridiagonalna{T <: Number}
    main :: Vector{T} # Glavna diagonala
    side :: Vector{T} # Stranska diagonala
end

struct SpodnjaTridiagonalna{T <: Number}
    x :: Vector{T} # Glavna diagonala
    y :: Vector{T} # Ena pod diagonalo
end

struct ZgornjaTridiagonalna{T <: Number}
    x :: Vector{T} # Glavna diagonala
    y :: Vector{T} # Ena nad diagonalo
end

Tridiag = Union{Tridiagonalna,SpodnjaTridiagonalna, ZgornjaTridiagonalna}
UpDownTridiag = Union{SpodnjaTridiagonalna, ZgornjaTridiagonalna}

# -------------------- [ Basic functions ] -------------------- #

firstindex(T :: Tridiagonalna) = 1

lastindex(T :: Tridiagonalna) = length(T.main)^2

size(T :: Tridiagonalna) = (length(T.main), length(T.main))
size(T :: UpDownTridiag) = (length(T.x), length(T.x))

function isapprox(T :: Tridiag, A)
    n,_ = size(T)
    return T*Matrix{Float64}(I,n,n) ≈ A
end

function to_matrix(T :: Tridiag)
    n,_ = size(T)
    return T*Matrix{Float64}(I,n,n)
end

# -------------------- [ Get index ] -------------------------- #

function getindex(T :: Tridiagonalna, i, j)
    n = length(T.main)
    if i > n || j > n || i < 1 || j < 1
        throw(BoundsError(T, [i,j]))
    end

    if i == j
        return T.main[i]
    elseif i == j-1
        return T.side[i]
    elseif i == j+1
        return T.side[j]
    else
        return 0
    end
end

function getindex(T :: SpodnjaTridiagonalna, i, j)
    n = length(T.x)
    if i > n || j > n || i < 1 || j < 1
        throw(BoundsError(T, [i,j]))
    end

    if i == j
        return T.x[i]
    elseif i == j+1
        return T.y[j]
    else
        return 0
    end
end

function getindex(T :: ZgornjaTridiagonalna, i, j)
    n = length(T.x)
    if i > n || j > n || i < 1 || j < 1
        throw(BoundsError(T, [i,j]))
    end

    if i == j
        return T.x[i]
    elseif i == j-1
        return T.y[i]
    else
        return 0
    end
end


# -------------------- [ Set index ] -------------------------- #

function setindex!(T :: Tridiagonalna, x, i, j)
    if i == j
        T.main[i] = x
    elseif i == j-1
        T.side[i] = x
    elseif i == j+1
        T.side[j] = x
    else
        throw(BoundsError(T, [i,j]))
    end
end

function setindex!(T :: SpodnjaTridiagonalna, x, i, j)
    if i == j
        T.x[i] = x
    elseif i == j+1
        T.y[j] = x
    else
        throw(BoundsError(T, [i,j]))
    end
end

function setindex!(T :: ZgornjaTridiagonalna, x, i, j)
    if i == j
        T.x[i] = x
    elseif i == j-1
        T.y[i] = x
    else
        throw(BoundsError(T, [i,j]))
    end
end


# ----------------- [ Matrix multiplication ] ----------------- #

function *(T :: Tridiagonalna, A)
    B = zero(A)
    n = length(T.main)
    nA,m = size(A)

    if nA != n
        throw(ArgumentError("Incompatable matrix dimension. Expected $n got $nA"))
    end

    for j = 1 : m
        B[1,j] = T[1,1] * A[1,j] +
               + T[1,2] * A[2,j]
    end
    for i = 2 : n-1
        for j = 1 : m
            B[i,j] = T[i,i-1] * A[i-1,j] +
                   + T[i,i]   * A[i,j] +
                   + T[i,i+1] * A[i+1,j]
        end
    end
    for j = 1 : m
        B[n,j] = T[n,n-1] * A[n-1,j] +
               + T[n,n]   * A[n,j]
    end

    return B
end


function *(T :: ZgornjaTridiagonalna, A)
    B = zero(A)
    n = length(T.x)
    nA,m = size(A)

    if nA != n
        throw(ArgumentError("Incompatable matrix dimension. Expected $n got $nA"))
    end

    for i = 1 : n-1
        for j = 1 : m
            B[i,j] = T[i,i]   * A[i,j] +
                   + T[i,i+1] * A[i+1,j]
        end
    end
    for j = 1 : m
        B[n,j] = T[n,n] * A[n,j]
    end

    return B
end


function *(T :: SpodnjaTridiagonalna, A)
    B = zero(A)
    n = length(T.x)
    nA,m = size(A)

    if nA != n
        throw(ArgumentError("Incompatable matrix dimension. Expected $n got $nA"))
    end

    for j = 1 : m
        B[1,j] = T[1,1] * A[1,j]
    end
    for i = 2 : n
        for j = 1 : m
            B[i,j] = T[i,i-1] * A[i-1,j] +
                   + T[i,i]   * A[i,j]
        end
    end

    return B
end


function *(T :: Tridiagonalna, x :: Vector)
    y = zero(x)
    n = length(T.main)

    if n != length(x)
        throw(ArgumentError("Incompatable vector size. Expected $n got $(length(x))"))
    end

    return [ T[1,1] * x[1] + T[1,2] * x[2]

           ; [  T[i,i-1] * x[i-1] +
              + T[i,i]   * x[i]   +
              + T[i,i+1] * x[i+1]
             for i in 2 : n-1 ]

           ; T[n,n-1] * x[n-1] + T[n,n] * x[n]
           ]
end

function *(T :: ZgornjaTridiagonalna, x :: Vector)
    y = zero(x)
    n = length(T.x)

    if n != length(x)
        throw(ArgumentError("Incompatable vector size. Expected $n got $(length(x))"))
    end

    return  [ [   T[i,i]   * x[i]   +
                + T[i,i+1] * x[i+1]
                for i in 1 : n-1 ]

            ; T[n,n] * x[n]
            ]
end

function *(T :: SpodnjaTridiagonalna, x :: Vector)
    y = zero(x)
    n = length(T.x)

    if n != length(x)
        throw(ArgumentError("Incompatable vector size. Expected $n got $(length(x))"))
    end

    return [ T[1,1] * x[1]

            ; [  T[i,i-1] * x[i-1] +
                + T[i,i]   * x[i]
                for i in 2 : n ]
            ]
end

# ----------------- [ Matrix multiplication ] ----------------- #

function \(A :: SpodnjaTridiagonalna, b)
    n, _ = size(A)
    x    = zero(b)

    x[1] = b[1]/A[1,1]
    for i = 2 : n
        x[i] = (b[i] - A[i,i-1] * x[i-1]) / A[i,i]
    end
    return x
end

function \(A :: ZgornjaTridiagonalna, b)
    n, _ = size(A)
    x    = zero(b)

    x[n] = b[n]/A[n,n]
    for i = n-1:-1:1
        x[i] = (b[i] - A[i,i+1] * x[i+1]) / A[i,i]
    end
    return x
end


# ----------------- [ LU razcep ] ----------------------------- #

function lu(A :: Tridiagonalna)
    n = length(A.main)

    L = SpodnjaTridiagonalna(ones(n), zeros(n-1))
    U = ZgornjaTridiagonalna(copy(A.main), copy(A.side))

    for k = 1:n-1
        l_ik = A[k+1,k] / U[k,k]

        U[k+1,k+1] = U[k+1,k+1] - l_ik*U[k,k+1]
        L[k+1,k] = l_ik
    end

    return L, U
end


# ---------------- [ Householderjeva zrcaljenja ] ------------- #

sgn(x) = isapprox(x, 0) ? 1 : sign(x)

"""
    T, Q = tridiag(A)

Napišite funkcijo tridiag, ki s Hausholderjevimi zrcaljenji poišče
tridiagonalno matriko H tipa Tridiagonalna, ki je podobna matriki A.
"""
function tridiag(A)
    n,m = size(A)
    if n != m
        throw(ArgumentError(A, "Expected square matrix"))
    end

    T  = copy(A)
    Q  = Matrix{Float64}(I,n,n)
    T,Q = promote(T,Q)

    for k = 1:n-2
        x1 = T[k+1,k]
        a  = sgn(x1) * norm(T[k+1:n,k], 2)
        r  = sqrt(2 * (a*a + a*x1))

        u  = [ T[k+1,k] + a; T[k+2:n,k] ] / r
        Hk = I - 2*u*u'

        Q[k:n, k+1:n] = Q[k:n, k+1:n] * Hk

        T[k+1:n,   k:n] = Hk * T[k+1:n, k:n]
        T[  k:n, k+1:n] = T[k:n, k+1:n] * Hk
    end

    main = zeros(n)
    side = zeros(n-1)
    for i = 1:n-1
        main[i] = T[i,i]
        side[i] = T[i+1,i]
    end
    main[n] = T[n,n]
    Tr = Tridiagonalna(main, side)

    return Tr, Q
end


# ---------------- [ Inverzna potenčna ] ---------------------- #

######## ORIGINALNA IMPLEMENTACIJA Z UPORABO norm(x,2)
# function inv_lastni(A, l, maxit=100, tol=1e-10)
#     n, _ = size(A)
#
#     T, Q = tridiag(A)
#
#     Tλ = Tridiagonalna(copy(T.main), T.side)
#     for i = 1 : n
#         Tλ[i,i] = Tλ[i,i] - l
#     end
#
#     L, U = lu(Tλ)
#     y    = rand(n)
#
#     for iter = 1 : maxit
#         y = U \ (L \ y)
#
#         y = normalize(y, 2)
#         l = y'*(T*y)
#
#         if norm(T*y - l*y, Inf) < tol
#             return l, Q*y
#         end
#     end
#     return 0, zeros(n)
# end

######## NOVA IMPLEMETACIJA Z UPORABO norm(x,Inf)
function inv_lastni(A, λ0, maxit=100, tol=1e-10)
    n, _ = size(A)

    T, Q = tridiag(A)

    Tλ = Tridiagonalna(copy(T.main), T.side)
    for i = 1 : n
        Tλ[i,i] = Tλ[i,i] - λ0
    end

    L, U = lu(Tλ)
    x    = rand(n)
    xk   = x

    for iter = 1 : maxit
        y = U\(L\x)

        _, m = findmax(abs, x)

        λ = x[m] / y[m] + λ0
        x =    y / y[m]

        if norm(T*x - λ*x, Inf) < tol
            return λ, Q*x
        end
        xk = x
    end
    return nothing
end


# ------------------------------------------------------------- #

end # module Domaca01
