# Domača naloga 1: Inverzna potenčna metoda za tridiagonalno matriko

<ins>Avtor:</ins> Žiga Leskovec

<ins>Datum:</ins> 13. 4. 2024

## Uvod

V domači nalogi je bilo potrebno izvedeti implementacijo inverzne potenčne metode za iskanje lastnih vrednosti matrike za tridiagonalne matrike.

Prvi del poročila predstavlja opis implementacije nove podatkovne strukture `Tridiagonalna` in funkcijo `tridiag`.
Nadaljuje s krajšim opisom dveh podstruktur `ZgornjaTridiagonalna` in `SpodnjaTridiagonalna` in opisom implementacij osnovnih funkcij, kot so `lu` in operator `\`.

V drugem delu se osredotočimo na inverzno potenčno metodo.
Podan je grob opis splošne metode in kakšne posebnosti se pojavijo ko jo uporabimo na simetričnih matrikah.

Na koncu predstavimo uporabo inverzne iteracije pri iskanju lastnih vrednosti na primeru laplaceove matrike.

## Projekt

- [docs](./docs): generiran pdf poročila z uporabo `jupyter notebook`
- [scripts](./scripts): testne skripte in poročilo napisano v `jupyter notebook`
- [src](./src): izvorna koda. V [Domaca01.jl](./src/Domaca01.jl) se nahaja izvorna koda domače naloge
- [test](./test): testi za domačo nalogo. Poženejo se tako kot pri vajah (`test Domaca01` v pkg)

IJulia kernel rabi bit nastavljen da uporabi lokalno okolje, saj samo tako lahko uporabimo `using Domaca01`.
V dokumentu [porocilo.ipynb](./scripts/porocilo.ipynb) se nahaja demo implementacije domače naloge.
