using Test, Domaca02

@testset "Vozlišča in uteži" begin
    @testset "Trapezno pravilo" begin
        x, w = trapez(0,4,5)
        @test x ≈ 0:4
        @test w ≈ [0.5, 1.0, 1.0, 1.0, 0.5]
    end

    @testset "Simpsonovo pravilo" begin
        x, w = simpson(0,4,5)
        @test x ≈ 0:4
        @test w ≈ [1/3, 4/3, 2/3, 4/3, 1/3]
    end
end

@testset "Metode numerične integracije" begin
    @testset "Rombergova metoda" begin
        @test romberg(sin, 0, pi, eps()) ≈ 2
    end

    @testset "Monte Carlo" begin
        rng() = rand(Float64)*pi
        @test abs(monte_carlo(sin, rng, pi, 1e7) - 2) < 1e3
    end

    @testset "Sestavljeno trapezno pravilo" begin
        x, w = trapez(0,pi,1000000)
        @test d_integral(x -> sin(x[1]), x, w, 1) ≈ 2
    end

    @testset "Sestavljeno simpsonovo pravilo" begin
        x, w = simpson(0,pi,101)
        @test d_integral(x -> sin(x[1]), x, w, 1) ≈ 2
    end
end