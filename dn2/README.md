# Domača naloga 2

<ins>Avtor:</ins> Žiga Leskovec

<ins>Datum:</ins> 4. 6. 2024

## Uvod

Domača naloga je sestavljena iz dveh delov.
Za prvi sem se odločil za računanje *porazdelitvene funkcije normalne slučajne spremelnjivke*
Zanjo je implementirana Rombergova metoda in v poročilu je podan opis izvedenih transformacij nad definicijo funkcije, ki so bili potrebni za uspešno numerično integriranje.

V drugem delu je izračunana *sila težnosti* med dvema homogenima enotskima kockama.
Zanjo so implementirane 3 metode, ena naključnostna (Monte Carlo) in dve deterministični (večdimenziskjo trapezno in Simpsonovo sestavljeno pravilo).
Narisani so tudi grafi, ki prikazujejo razliko v rezultatih med tremi metodami.

## Projekt

- [docs](./docs): generiran pdf poročila z uporabo `jupyter notebook`
- [scripts](./scripts): testne skripte in poročilo napisano v `jupyter notebook`
- [src](./src): izvorna koda. V [Domaca02.jl](./src/Domaca02.jl) se nahaja izvorna koda domače naloge
- [test](./test): testi za domačo nalogo. Poženejo se tako kot pri vajah (`test Domaca02` v pkg)

IJulia kernel rabi bit nastavljen da uporabi lokalno okolje, saj samo tako lahko uporabimo `using Domaca02`.
V dokumentu [porocilo.ipynb](./scripts/porocilo.ipynb) se nahaja demo implementacije domače naloge.