module Domaca02

export romberg, monte_carlo, d_integral, trapez, simpson


function trapez(a, b, n)
    x = collect(range(a,b,n))
    h = x[2] - x[1]

    w      = repeat([2], length(x))
    w[1]   = 1
    w[end] = 1
    
    w *= h/2
    
    return x, w
end


function simpson(a, b, n)
    if n % 2 == 0
        throw(ArgumentError("n: Pričakovano liho število, dobljeno sodo ($n)"))
    end

    x = collect(range(a,b,n))
    h = x[2] - x[1]

    w = ones(n)
    for i = 2:n-1
        if i % 2 == 0
            w[i] = 4
        else
            w[i] = 2
        end
    end
    w *= h/3

    return x, w
end

"""
    I = trapezno(f, a, b, h)

Izračunaj integral funkcije `f` s sestavljenim korakom
na intervalu `[a, b]` s korakom `h`
"""
function trapezno(f, a, b, h)
    X = a:h:b
    Y = map(f, X) * h
    Y[1] /= 2
    Y[end] /= 2

    return sum(Y)
end

"""
    I = romberg(f, a, b, eps, max_iter)

Izračunaj integral funkcije `f` z Rombergovo metodo
na intervalu `[a, b]`, toleranco `eps` in največ `max_iter`
iteracijami 
"""
function romberg(f, a, b, eps, max_iter=1000)
    h    = b - a
    T(h) = trapezno(f, a, b, h)

    tp = zeros(max_iter)
    t  = zeros(max_iter)

    tp[1] = T(h)
    for l = 2 : max_iter
        h    = h / 2
        t[1] = T(h)

        pow4 = 4
        for m = 2 : l
            t[m] = (pow4 * t[m-1] - tp[m-1]) / (pow4-1)
            pow4 *= 4
        end

        if abs(t[l] - tp[l-1]) < eps
            return t[l]
        end

        tp, t = t, tp
    end

    return t[max_iter]
end

"""
    I = monte_carlo(f, rand, vol, eps, max_iter)

Izračunaj integral več dimenzijske funkcije `f` na območju vzorčenim z `rand`,
toleranco `eps` in največ `max_iter` koraki
"""
function monte_carlo(f, rand, vol, max_iter=1000)
    S = zero(f(rand()))
    n = 0
    I = NaN
    for _ = 1 : max_iter
        n += 1
        S = S .+ f(rand())
        
        E = S / n
        I = E * vol
    end

    return I
end


"""
    I = d_integral(f, x, w, d)

Izračuna integral funkcije `f` na d-dimenzionalni kocki `[a,b]^d`,
kjer je interval `[a,b]` podan implicitno z vektorjem vrzelov `x` in
`w` predstavlja vektor uteži. 
"""
function d_integral(f, x, w, d)
    n = length(x)
    i = ones(Int, d)

    I = zero(f(zero(x[i])))
    for _ = 1 : n^d
        I += prod(w[i]) * f(x[i]) 
        
        for _i = 1:d
            if i[_i] < n
                i[_i] += 1
                break
            else
                i[_i] = 1
            end
        end
    end

    return I
end

end # module Domaca02
