using Domaca02

n = 5

x = collect(range(0, 1, n))
h = x[2] - x[1]
w = repeat([2], n)
w[1] = 1
w[end] = 1
w *= h/2

function F(x)
    diff = x[1:3] - x[4:6] - [2,0,0]
    r    = diff / (diff[1]^2 + diff[2]^2 + diff[3]^2)
    return r
end

d_integral(F, x, w, 6)